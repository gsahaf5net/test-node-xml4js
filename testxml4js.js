var fs = require('fs'),
    xml4js = require('xml4js'),
    util = require('util');

var processAttb = function(name) {
    return '@attr:'+name
}
var processTag = function(name) {
    console.log("in processAttb")
    return name
}

var options = {
    explicitRoot: false,
    explicitArray: false,
    mergeAttrs: true,
    preserveChildrenOrder: true
};
var parser = new xml4js.Parser(options);
var schema = fs.readFileSync('import_export_xml_partial.xsd', {encoding: 'utf-8'});

fs.readFile('small_partial_test_policy.xml', function(err, data) {
    parser.addSchema('http://www.w3.org/2001/XMLSchema', schema, function (err, importsAndIncludes) {
        console.log(util.inspect(importsAndIncludes, false, null));
        if (err){
            console.log('error in 1st schema' + err)
        }
        else if (importsAndIncludes){
            console.log('adding 2nd schema');
            schema2 = fs.readFileSync('negsig/sets.xsd', {encoding: 'utf-8'});
            parser.addSchema('http://www.w3.org/2001/XMLSchema', schema2, function (err, importsAndIncludes) {
                console.log(util.inspect(importsAndIncludes, false, null));
                if (err){
                    console.log('error in 2nd schema' + err)
                }
                else if (importsAndIncludes){
                    console.log('adding 3rd schema');
                    schema3 = fs.readFileSync('negsig/common.xsd', {encoding: 'utf-8'});
                    console.log('after 3rd schema fileread');
                    parser.addSchema('http://www.w3.org/2001/XMLSchema', schema3, function (err, importsAndIncludes) {
                        console.log('3rd schema callback : ');
                        //console.log(util.inspect(importsAndIncludes, false, null));
                        if (err){
                            console.log('error in 3rd schema' + err)
                        }
                        else if (!importsAndIncludes){
                            
                            parser.parseString(data, function(err, result){
                                if (!err) {
                                    fs.writeFile('small_partial_test_policy.json',
                                        util.inspect(result, false, null),
                                        function(){
                                            console.log('Writing JSON file Done');
                                        }
                                    )
                                }
                                else {
                                    console.log('parsing error:'+err);
                                }
                            })
                        }
                    })
                }
            })
        }
    })
})
